#pragma once
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include "Cordinate.h"
#include <algorithm>
#include <map>
#include <queue>
using namespace std;
class Map
{
	private:
		vector<string> SplitString(string str, char byChar);
		struct queueNode {
			Cordinate pt;
			int dist;
			vector<Cordinate> road;
		};
	public:
		vector<vector<bool>> data;
		vector<Cordinate> ShortestPath(Cordinate start, Cordinate destination);
		std::map<char, Cordinate> Doors;
		std::map<string, char> BadCharacterInfos;
		bool AnyWallAt(int x, int y);
		Map();
};