#pragma once
#include <string>
#include "Cordinate.h"
#include "Map.h"
using namespace std;
class Player
{
protected:
	string Image;
	bool IsGood;
	Map Map;
private:
	int MovementSpeed = 1;
	Cordinate Position;
	string Name;
public:
	Cordinate StartPosition;
	virtual vector<Cordinate> ShortestPath(Cordinate destination);
	vector<Cordinate> Path;
	void setPosition(int row, int colunm);
	virtual void moveUp();
	virtual void moveLeft();
	virtual void moveRight();
	virtual void moveDown();
	virtual bool DecreaseHealth();
	Cordinate currentCordinate() {
		return Position;
	}
	string ImagePath();
	Player(Cordinate pos, ::Map map);
};