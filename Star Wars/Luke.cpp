#include "Luke.h"
Luke::Luke(Cordinate pos, ::Map map) : GoodPlayer(pos, map) {
	this->Image = "luke.png";
}
bool Luke::DecreaseHealth() {
	this->LeftHealth -= 1;
	cout << "Left Health: " << this->LeftHealth << endl;
	if (this->LeftHealth == 0) return 1;
	return 0;
}