#include "Player.h"
Player::Player(Cordinate pos, ::Map map) {
	this->Position = pos;
	this->StartPosition = pos;
	this->Map = map;
}
vector<Cordinate> Player::ShortestPath(Cordinate destination) {
	vector<Cordinate> path = this->Map.ShortestPath(this->currentCordinate(), destination);
	if(path.size() >=  2) this->setPosition(path.at(1).row, path.at(1).column);
	else if (path.size() == 1) this->setPosition(path.at(0).row, path.at(0).column);
	this->Path = path;
	return path;
}

string Player::ImagePath() {
	return this->Image;
}

void Player::setPosition(int row, int colunm)
{
	this->Position.row = row;
	this->Position.column = colunm;
}

void Player::moveUp() {
	if(!this->Map.AnyWallAt(this->Position.row - 1, this->Position.column))
		this->Position.row -= this->MovementSpeed;
}

void Player::moveDown() {
	if (!this->Map.AnyWallAt(this->Position.row + 1, this->Position.column))
		this->Position.row += this->MovementSpeed;
}

bool Player::DecreaseHealth()
{
	return false;
}

void Player::moveLeft() {
	if (!this->Map.AnyWallAt(this->Position.row, this->Position.column - 1))
		this->Position.column -= this->MovementSpeed;
}

void Player::moveRight() {
	if (!this->Map.AnyWallAt(this->Position.row, this->Position.column + 1))
		this->Position.column += this->MovementSpeed;
}