#include "Display.h"
#include "Player.h"
Display::Display(::Map* mapData, Player* goodPlayer, vector<Player*> badPlayers) {
	this->GoodPlayer = goodPlayer;
	this->BadPlayers = badPlayers;
	this->Map = mapData;
	al_init();
	al_init_primitives_addon();
	al_init_image_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	int width = padding * 2 + Map->data.at(0).size() * boxSize;
	int height = padding * 2 + Map->data.size() * boxSize;
	this->screen = al_create_display(width, height);
	al_clear_to_color(al_map_rgb(255, 255, 255));
	al_flip_display();
	this->DrawMap();
	this->ListenEvents();
}
void Display::ListenEvents() {
	ALLEGRO_DISPLAY *display = this->screen;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	bool key[4] = { false, false, false, false };
	bool redraw = true;
	bool doexit = false;
	if (!al_install_keyboard()) {
		fprintf(stderr, "failed to initialize the keyboard!\n");
		return;
	}
	timer = al_create_timer(1.0 / 10);
	if (!timer) {
		fprintf(stderr, "failed to create timer!\n");
		return;
	}
	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "failed to create event_queue!\n");
		al_destroy_display(display);
		al_destroy_timer(timer);
		return;
	}
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_flip_display();
	al_start_timer(timer);
	while (!doexit)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		vector<vector<Cordinate>> paths;
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			if (key[KEY_UP]) {
				this->GoodPlayer->moveUp();
				for (auto& badPlayer : this->BadPlayers) {
					badPlayer->ShortestPath(this->GoodPlayer->currentCordinate());
				}
				Control();
			}
			if (key[KEY_DOWN]) {
				this->GoodPlayer->moveDown();
				for (auto& badPlayer : this->BadPlayers) {
					badPlayer->ShortestPath(this->GoodPlayer->currentCordinate());
				}
				Control();
			}
			if (key[KEY_LEFT]) {
				this->GoodPlayer->moveLeft();
				for (auto& badPlayer : this->BadPlayers) {
					badPlayer->ShortestPath(this->GoodPlayer->currentCordinate());
				}
				Control();
			}
			if (key[KEY_RIGHT]) {
				this->GoodPlayer->moveRight();
				for (auto& badPlayer : this->BadPlayers) {
					badPlayer->ShortestPath(this->GoodPlayer->currentCordinate());;
				}
				Control();
			}
			redraw = true;
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			break;
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = true;
				break;

			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = true;
				break;

			case ALLEGRO_KEY_LEFT:
				key[KEY_LEFT] = true;
				break;

			case ALLEGRO_KEY_RIGHT:
				key[KEY_RIGHT] = true;
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = false;
				break;
			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = false;
				break;
			case ALLEGRO_KEY_LEFT:
				key[KEY_LEFT] = false;
				break;
			case ALLEGRO_KEY_RIGHT:
				key[KEY_RIGHT] = false;
				break;
			case ALLEGRO_KEY_ESCAPE:
				doexit = true;
				break;
			}
		}
		if (redraw && al_is_event_queue_empty(event_queue)) {
			redraw = false;
			al_clear_to_color(al_map_rgb(255, 255, 255));
			this->DrawMap();
		}
	}
}
void Display::DrawMap() {
	ALLEGRO_FONT *sec = al_load_ttf_font("comic.ttf", 25, 0);
	ALLEGRO_BITMAP *goodChar = al_load_bitmap(this->GoodPlayer->ImagePath().c_str());
	int currentRow = 0;
	for (auto &row : this->Map->data) {
		int currentColunm = 0;
		for (auto isWall : row) {
			int leftUpperX = padding + boxSize * currentColunm;
			int leftUpperY = padding + boxSize * currentRow;
			int rightDownX = leftUpperX + boxSize;
			int rightDownY = leftUpperY + boxSize;
			if (isWall == false) {
				al_draw_rectangle(leftUpperX, leftUpperY, rightDownX, rightDownY, al_map_rgb(25, 24, 25), 1);
				if (currentColunm == 13 && currentRow == 9) {
					al_draw_filled_rectangle(leftUpperX, leftUpperY, rightDownX, rightDownY, al_map_rgb(204, 204, 0));
				}
				for (auto it = this->Map->Doors.begin(); it != this->Map->Doors.end(); ++it)
					if (it->second.row == currentRow && it->second.column == currentColunm) {
						al_draw_filled_rectangle(leftUpperX, leftUpperY, rightDownX, rightDownY, al_map_rgb(0, 0, 204));
						al_draw_text(sec, al_map_rgb(255, 255, 255), (leftUpperX + rightDownX) / 2, leftUpperY, ALLEGRO_ALIGN_CENTRE, &it->first);
					}
			}
			else al_draw_filled_rectangle(leftUpperX, leftUpperY, rightDownX, rightDownY, al_map_rgb(224, 102, 255));
			currentColunm++;
		}
		currentRow++;
	}
	for (auto& badPlayer : this->BadPlayers) {
		this->DrawPath(badPlayer->Path);
	}
	ALLEGRO_BITMAP  *cup = NULL;
	cup = al_load_bitmap("cup.jpg");
	al_draw_bitmap(cup, 540, 380, 0);
	Cordinate goodPlayerCordinate = this->GoodPlayer->currentCordinate();
	int goodPlayerX = padding + boxSize * goodPlayerCordinate.column;
	int goodPlayerY = padding + boxSize * goodPlayerCordinate.row;
	for (auto& badPlayer : this->BadPlayers) {
		Cordinate badPlayerPlayerCordinate = badPlayer->currentCordinate();
		int badPlayerX = padding + boxSize * badPlayerPlayerCordinate.column;
		int badPlayerY = padding + boxSize * badPlayerPlayerCordinate.row;
		al_draw_filled_rectangle(badPlayerX, badPlayerY, badPlayerX + this->boxSize - 1, badPlayerY + this->boxSize - 1, al_map_rgb(255, 255, 255));
		ALLEGRO_BITMAP *badChar = al_load_bitmap(badPlayer->ImagePath().c_str());
		al_draw_bitmap(badChar, badPlayerX, badPlayerY, 0);
		al_destroy_bitmap(badChar);
	}
	al_draw_bitmap(goodChar, goodPlayerX, goodPlayerY, 0);
	al_flip_display();
	al_destroy_bitmap(goodChar);
	al_destroy_bitmap(cup);
	al_destroy_font(sec);
}

bool Display::Control() {
	if (this->GoodPlayer->currentCordinate().row == 9 && this->GoodPlayer->currentCordinate().column == 13) {
		cout << "!YOU WIN!" << endl;
		ALLEGRO_BITMAP  *win = NULL;
		al_init_image_addon();
		win = al_load_bitmap("win.jpg");
		al_draw_bitmap(win, -20, -75, 0);
		al_flip_display();
		system("pause");
		exit(1);
	}
	for (auto& player : this->BadPlayers) {
		if (this->GoodPlayer->currentCordinate().row == player->currentCordinate().row && this->GoodPlayer->currentCordinate().column == player->currentCordinate().column) {
			cout << "CAUGHT!" << endl;
			if (this->GoodPlayer->DecreaseHealth()) {
				ALLEGRO_BITMAP  *gameover = NULL;
				al_init_image_addon();
				gameover = al_load_bitmap("gameover.png");
				al_draw_bitmap(gameover, -275, -100, 0);
				al_flip_display();
				system("pause");
				exit(1);
			}
			else {
				this->GoodPlayer->setPosition(this->GoodPlayer->StartPosition.row, this->GoodPlayer->StartPosition.column);
				for (auto& badPlayer : this->BadPlayers) {
					badPlayer->setPosition(badPlayer->StartPosition.row, badPlayer->StartPosition.column);
					badPlayer->Path.clear();
				}
			}
		}
	}
	return 0;
}
void Display::DrawPath(vector<Cordinate> cordinates) {
	int i = 0;
	for (auto& cordinate : cordinates) {
		if (i == 0) {
			i++;
			continue;
		}
		int leftUpperX = padding + boxSize * cordinate.column;
		int leftUpperY = padding + boxSize * cordinate.row;
		int rightDownX = leftUpperX + boxSize;
		int rightDownY = leftUpperY + boxSize;
		al_draw_filled_rectangle(leftUpperX, leftUpperY, rightDownX, rightDownY, al_map_rgb(255, 0, 0));
	}
}
Display::~Display() {
	al_destroy_display(screen);
}
