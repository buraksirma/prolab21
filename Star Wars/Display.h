#pragma once
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_native_dialog.h>
#include <vector>
#include "Map.h"
#include "Player.h"
using namespace std;
class Display {
	private:
		enum MYKEYS {
			KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT
		};
		ALLEGRO_DISPLAY* screen;
		Map* Map;
		Player* GoodPlayer;
		vector<Player*> BadPlayers;
		//Padding from all sides 80 px;
		int padding = 20;
		//An box is 40x40
		int boxSize = 40;
	public:
		Display(::Map* mapData, Player* goodPlayer, vector<Player*> badPlayers);
		void ListenEvents();
		void DrawMap();
		~Display();
		bool Control();
		void DrawPath(vector<Cordinate> cordinates);
};
