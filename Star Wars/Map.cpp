#include "Map.h"
Map::Map() {
	ifstream file("map.txt");
	if (file.is_open()) {
		string line;
		int currentRow = 0;
		char CurrentDoor = 'A';
		char Badcharassembler = 'A';
		bool readingCharacters = true;
		while (getline(file, line)) {
			if (line.empty()) {
				readingCharacters = false;
				continue;
			}
			if (!readingCharacters) {

				vector<bool> thisRow;
				int currentColunm = 0;
				vector<string> splitted = SplitString(line, '\t');
				for (auto &isWall : splitted)
				{
					if ((currentColunm == 0 || currentRow == 0 || currentColunm == 13 || currentRow == 10) && isWall == "1" && !(currentColunm == 13 && currentRow == 9)) {
						this->Doors[CurrentDoor] = Cordinate(currentRow, currentColunm);
						CurrentDoor++;
					}
					if (isWall == "0") thisRow.push_back(true);
					else thisRow.push_back(false);
					currentColunm++;
				}
				this->data.push_back(thisRow);
				currentRow++;
			}
			else {
				vector<string> splitbycomma = SplitString(line, ',');
				string character = splitbycomma.at(0);
				character = SplitString(character, ':').at(1);
				character += Badcharassembler;
				Badcharassembler++;
				string door = splitbycomma.at(1);
				door = SplitString(door, ':').at(1);
				std::transform(character.begin(), character.end(), character.begin(), ::tolower);
				std::transform(door.begin(), door.end(), door.begin(), ::toupper);
				this->BadCharacterInfos[character] = door.at(0);
			}
		}
		file.close();
	}
}

vector<string> Map::SplitString(string str, char byChar)
{
	stringstream strtostream(str);
	string segment;
	vector<string> seglist;
	while (getline(strtostream, segment, byChar))
	{
		if (segment.empty()) continue;
		seglist.push_back(segment);
	}
	return seglist;
}

vector<Cordinate> Map::ShortestPath(Cordinate start, Cordinate destination)
{
	int rowNum[] = { -1, 0, 0, 1 };
	int colNum[] = { 0, -1, 1, 0 };
	//if (!this->data[start.row][start.column] || !this->data[destination.row][destination.column])	throw "Cant access.";
	vector<vector<bool>> visited(this->data.size(), vector<bool>(this->data.at(0).size(), false));
	visited[start.row][start.column] = true;
	queue<queueNode> q;
	queueNode s = { start, 0 };
	q.push(s);
	while (!q.empty())
	{
		queueNode curr = q.front();
		Cordinate pt = curr.pt;
		if (pt.row == destination.row && pt.column == destination.column) {
			curr.road.push_back(curr.pt);
			return curr.road;
		}
		q.pop();
		for (int i = 0; i < 4; i++)
		{
			int row = pt.row + rowNum[i];
			int col = pt.column + colNum[i];
			if ((row >= 0) && (row < this->data.size()) && (col >= 0) && (col < this->data.at(0).size()) && !this->data[row][col] && !visited[row][col]) {
				visited[row][col] = true;
				queueNode Adjcell = { {row, col},
									  curr.dist + 1 };
				Adjcell.road = curr.road;
				Adjcell.road.push_back(curr.pt);
				q.push(Adjcell);
			}
		}
	}
	throw "Cant access.";
}

bool Map::AnyWallAt(int x, int y) {
	if (x == 9 && y == 13) return false;
	if (y == 0 || y == this->data.at(0).size() - 1 || x == 0 || x == this->data.size() - 1) return true;
	if (this->data.at(x).at(y) == true) return true;
	else return false;
}
