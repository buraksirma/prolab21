#include <iostream>
#include "Display.h"
#include "Map.h"
#include "Luke.h"
#include "Yoda.h"
#include "Trooper.h"
#include "Kylo.h"
#include "Vader.h"
using namespace std;

void main() {
	int selection;
	cout << "Karakter seciniz: " << endl;
	cout << "1 -> Luke Skywalker" << endl;
	cout << "2 -> MasterYoda" << endl;
	cin >> selection;
	Map* labyrinth = new Map();
	Cordinate start(labyrinth->data.size()/2, labyrinth->data.at(0).size() / 2 - 1);
	Player* GoodPlayer;
	vector<Player*> BadPlayers;
	if (selection == 2) {
		Yoda* master = new Yoda(start, *labyrinth);
		GoodPlayer = master;
	}
	else {
		Luke* skywalker = new Luke(start, *labyrinth);
		GoodPlayer = skywalker;
	}
	for (auto& character : labyrinth->BadCharacterInfos)
	{
		Cordinate badStart = labyrinth->Doors.at(character.second);
		string name = character.first;
		name = name.substr(0, name.size() - 1);
		if (name == "stormtrooper") {
			Trooper* trooper = new Trooper(badStart, *labyrinth);
			BadPlayers.push_back(trooper);
		}
		else if (name == "kyloren") {
			Kylo* kylo = new Kylo(badStart, *labyrinth);
			BadPlayers.push_back(kylo);
		}
		else if (name == "darthvader") {
			Vader* vader = new Vader(badStart, *labyrinth);
			BadPlayers.push_back(vader);
		}
	}
	Display* screen = new Display(labyrinth, GoodPlayer, BadPlayers);
}
