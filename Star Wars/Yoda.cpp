#include "Yoda.h"
Yoda::Yoda(Cordinate pos, ::Map map) : GoodPlayer(pos, map) {
	this->Image = "yoda.png";
}
bool Yoda::DecreaseHealth() {
	this->LeftHealth -= 0.5;
	cout << "Left Health: " << this->LeftHealth << endl;
	if (this->LeftHealth == 0) return 1;
	return 0;
}